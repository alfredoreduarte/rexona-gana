var path = require('path')
var express = require('express')
var webpack = require('webpack')
var config = require('./webpack.config.dev')

const isDeveloping = process.env.NODE_ENV !== 'production'
var port;
if (isDeveloping) {
	port = 4006
}
else{
	port = 4007
}
process.env.PORT = process.env.PORT || port;
process.env.HOST = process.env.HOST || 'localhost';

const app = express()
app.set('view engine', 'ejs')
const compiler = webpack(config)

// Hot Module Reloading
// if (isDeveloping) {
	app.use(require('webpack-dev-middleware')(compiler, {
		noInfo: true,
		publicPath: config.output.publicPath
	}));
	app.use(require('webpack-hot-middleware')(compiler))
// }
// else{
	// app.use('/static',  express.static(__dirname + '/dist'))
// }

// Images and other static asssets
app.use('/images',  express.static(__dirname + '/assets/images'))
app.use('/styles',  express.static(__dirname + '/assets/styles'))
app.use('/uploads',  express.static(__dirname + '/uploads'))
app.use('/photos.zip',  express.static(__dirname + '/photos.zip'))
app.use('/node_modules',  express.static(__dirname + '/node_modules'))

// FORM DATA POST
// DB
var mysql = require('mysql')
var mysqluser
var mysqlpass
var database
if (isDeveloping) {
	if (process.env.DEV_MODE == 'true') {
		mysqlpass = '';
		mysqluser = 'root';
	}
	else{
		mysqlpass = 'aFLh"Bg7}=dKjW*K';
		mysqluser = 'rexonaganadev';
	}
	database = 'rexona-gana';
}
else{
	mysqluser = 'rexonaganadev';
	mysqlpass = 'rexooo';
	database = 'rexonaganadev';
}
var connection = null
var dbConfig = {
	host     : 'localhost',
	user     : mysqluser,
	password : mysqlpass,
	database : database
}
function handleDisconnect(){
	connection = mysql.createConnection(dbConfig);
	connection.connect(function(err){
		if(err){
			console.log('Error while connecting to db: ', err)
			setTimeout(handleDisconnect, 2000)
		}
	})
	connection.on('error', function(err){
		console.log('db error', err)
		if (err.code == 'PROTOCOL_CONNECTION_LOST') {
			handleDisconnect()
		}
		else {
			throw err
		}
	})
}
handleDisconnect();
// DB
// Upload
// POST from form
var multer  = require('multer')
var storage = multer.diskStorage({
	destination: 'uploads/',
	filename: function (req, file, cb) {
		var extension = ''
		switch(file.mimetype){
			case 'image/jpeg':
				extension = 'jpeg'
				break
			case 'image/png':
				extension = 'png'
				break
			case 'image/gif':
				extension = 'gif'
				break
		}
		cb(null, file.fieldname + '-' + Date.now() + '.' + extension)
	}
});
var upload = multer({ storage: storage })
var fs = require('fs')
var gm = require('gm').subClass({imageMagick: true})
// Upload
const bodyParser = require('body-parser')
const jsonParser = bodyParser.json({limit: '5mb'})
app.post('/savedata', jsonParser, function(req, res, next) {
	var data = JSON.stringify(req.body)
	var documentNumber = JSON.parse(data).document
	var post = {
		data: data,
		document: documentNumber
	}
	var response = {
		status: 'ok'
	}
	connection.query("INSERT INTO users SET ?", post, function(err, rows, fields) {
		if (err) throw err
		connection.query("SELECT COUNT(*) FROM users where document = " + documentNumber, function(err, rows, fields) {
			var response = {
				status: 'ok',
				couponCount: rows[0]['COUNT(*)']
			}
			res.send(response)
		})
	})
})
app.post('/upload', upload.single('photo'), function(req, res, next) {
	gm('uploads/' + req.file.filename)
		.autoOrient()
		.write('uploads/' + req.file.filename, function (err) {
			if (err){
				console.log('orient error')
				console.log(err)
			}
			else{
				var response = {
					status: 'ok',
					fileName: req.file.filename
				}
				res.send(response);
			}
		})
})

app.get('/dashdata', function(req, res) {
	connection.query("SELECT * from users", function(err, rows, fields) {
		if (err) throw err

		var response = {
			data: rows
		}

		res.send(response)
	})
})

// Download photos
app.get('/download-attachments', function(req, res) {
	var file_system = require('fs');
	var archiver = require('archiver');

	var output = file_system.createWriteStream('photos.zip');
	var archive = archiver('zip');

	output.on('close', function () {
	    console.log(archive.pointer() + ' total bytes');
	    console.log('archiver has been finalized and the output file descriptor has closed.');
	    res.render('download')
	});

	archive.on('error', function(err){
	    throw err;
	});

	archive.pipe(output);
	archive.bulk([
	  {
	    expand: true,
	    cwd: __dirname + "/uploads",
	    src: ["**/*"],
	    dot: true
	  }
	]);
	archive.finalize();	
})


// Serving static HTML
app.get('/*', function(req, res){
	res.render('index')
})

// Running the server
app.listen(process.env.PORT, function(err){
	if (err) {
		console.log(err)
		return;
	}
	console.log("Express server listening on port %d in %s mode", this.address().port, app.settings.env);
})