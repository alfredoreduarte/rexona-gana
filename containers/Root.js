import React, { Component } from 'react'
import { connect } from 'react-redux'
require('assets/styles/styles.sass')

class Root extends Component {
	render(){
		return (
			<div className="container">
				<p className="text-center">
					<img src="/images/title.png" className="img-responsive title" style={{margin: 'auto'}} />
				</p>
				<div className="texture"></div>
				<div className="col-sm-12">
					{React.cloneElement(this.props.children, { ...this.props})}
				</div>
			</div>
		)
	}
}

Root.path = '/'

export default connect()(Root)