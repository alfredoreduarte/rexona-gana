import React from 'react'
import { connect } from 'react-redux'
import { push } from 'react-router-redux'
import { FormGroup, FormControl, Button, HelpBlock } from 'react-bootstrap'
import { reduxForm } from 'redux-form'
import Footer from 'components/Footer'

let BrowserRedirection = () => (
	<div>
		<div className="vertiflex">
			<div className="">
				<div className="car"><img src="/images/car.png" /></div>
				<p className="text-center">
					<a 
						href="googlechrome://navigate?url=http://ganaconrexona.com" 
						style={{boxShadow: '0px 0px 10px rgba(0,0,0,.5)'}}
						className="btn btn-lg btn-default text-uppercase">Continuar</a>
				</p>
			</div>
		</div>
	</div>
)

export default BrowserRedirection