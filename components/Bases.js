import React from 'react'
import { connect } from 'react-redux'
import { FormGroup, FormControl, Button } from 'react-bootstrap'
import Footer from 'components/Footer'
import { Link } from 'react-router'

const Bases = () => (
	<div className="row">
		<div className="col-xs-12 panel panel-default" style={{background: 'white', color: 'black'}}>
			<div class="panel-body">
				<h2 className="text-center">Bases y Condiciones</h2>
				<p className="text-center">
					<strong>Promo: “Gana con Rexona”</strong>
				</p>
				<p>
					1. Podrán participar de la presente promoción denominada “Gana con Rexona” (en adelante la “Promoción”), organizada por Unilever de Paraguay S.A. (en
					adelante el “Organizador”), con domicilio en la calle Río Salado 316 y Río Montelindo, de la Ciudad de Villa Elisa, Paraguay, para su marca “REXONA”.
					Podrán participar de la presente promoción todas aquellas personas mayores de 18 años que estén domiciliados o residan en la República del Paraguay en
					adelante los “Participante/s”), que sigan el mecanismo de participación que se describe en las presentes Bases y Condiciones (en adelante las BASES).
				</p>
				<p>
					<strong>2. VIGENCIA:</strong>
					La Promoción tendrá vigencia desde el 24 de agosto hasta el 31 de Octubre de 2016 en la cadena de supermercados Stock, (en adelante “Plazo de Vigencia”), desarrollándose en la cadena Stock del grupo RETAIL
					S.A. todo el ámbito de la República del Paraguay. Este Plazo de Vigencia es improrrogable, no aceptándose en consecuencia participaciones posteriores a la
					finalización del mismo.
				</p>
				<p>
					3. No podrán participar de la Promoción, ni hacerse acreedores de los premios, el personal del Organizador, ni el personal de las empresas o sociedades
					vinculadas con éste, sus Agencias de Promoción y demás personas que el Organizador contrate para proveer cualquier producto o cualquier servicio
					relacionado con la Promoción y/o familiares directos de los mismos, como tampoco podrán participar los parientes por consanguinidad o afinidad hasta el
					segundo grado inclusive.
				</p>
				<p>
					<strong>4.</strong>
					<strong>MECÁNICA DE PARTICIPACIÓN:</strong>
					Durante la Vigencia de la Promoción, los Participantes deberán ingresar desde sus celulares o cualquier dispositivo con acceso a internet al dominio
					ganaconrexona.com y completar los campos de información solicitados, los cuales serían:
				</p>
				<p>
					• Foto del ticket de compra del producto Rexona, durante el periodo del 24 de agosto del 2016 al 31 de octubre del 2016 de cualquier comercio de la cadena
					de Stock y durante el periodo de 2 de noviembre del 2016 al 5 de diciembre del 2016 de cualquier comercio, los comercios deben estar
					ubicado dentro del territorio Paraguayo, donde se haga visible un desodorante de la marca Rexona.
				</p>
				<p>
					• Nombre del participante
				</p>
				<p>
					• Cedula de Identidad
				</p>
				<p>
					• Número de teléfono para contacto.
				</p>
				<p>
					• Fecha del Ticket de compra
				</p>
				<p>
					<strong>5. Sorteo.</strong>
					<strong>1er sorteo:</strong>
					Con fecha 1 de noviembre del 2016 a las 18:00hs hs se unificarán en una única base de datos, todas las participaciones realizadas durante el plazo de
					vigencia y se llevará a cabo el 2 de noviembre del 2016 un sorteo en La Escribanía Narvaja (Fulgencio R. Moreno esquina México) ante la presencia de un
					escribano público a los fines de determinar los potenciales ganadores (en adelante el “Sorteo”).
				</p>
				<p>
					<strong>2do sorteo:</strong>
					Con fecha 6 de diciembre de 2016 a las 18:00hs hs. se unificarán en una única base de datos, todas las participaciones realizadas durante el plazo de
					vigencia y se llevará a cabo 7 de diciembre de 2016 sorteo en La Escribanía Narvaja (Fulgencio R. Moreno esquina México) ante la presencia de un escribano
					público a los fines de determinar los potenciales ganadores (en adelante el “Sorteo”).
				</p>
				<p>
					La forma de realización del sorteo será al azar, a través de formula pandémica aleatoria. Se sorteará 1(uno) potenciales ganadores con sus correspondientes
					suplentes para el eventual caso que el posible ganador no cumpla con los requisitos aquí establecidos.
				</p>
				<p>
					El Organizador podrá modificar la fecha y el lugar del Sorteo, para lo cual deberá contar previamente con la autorización de la CONAJZAR y comunicar a los
					participantes mediante el mismo sistema utilizado para publicitar las presentes Bases.
				</p>
				<p>
					Cada potencial ganador no podrá acceder a más de un premio de los establecidos para la presente Promoción, sin importar la cantidad de veces que haya
					participado.
				</p>
				<p>
					<strong>6. PREMIOS DE LA PROMOCIÓN:</strong>
					EL premio establecido para la presente Promoción consiste en:
				</p>
				<p>
					1. Un automóvil Marca KIA Modelo Picanto Año 2016 0 km para la cadena Stock.
				</p>
				<p>
					<strong>7. MECÁNICA DE ENTREGA:</strong>
					El Organizador se comunicará con los ganadores los días previamente definidos en la FanPage de Rexona Py, donde se les informará su calidad de potencial
					ganador, el Premio y la mecánica de entrega en presencia de Escribano Público Nacional designado por el organizador. Asimismo, los posibles ganadores serán
					publicados en el muro de la FanPage.
				</p>
				<p>
					El organizador tendrá los premios asignados a disposición del ganador por el plazo establecido en Ley, es decir 60(sesenta) días. Los premios no asignados
					en el caso de haberlos, serán donados el 50 % a la entidad de beneficencia y los restantes 50% al financiamiento del FONARESS.
				</p>
				<p>
					La asignación del Premio está condicionada a la comprobación de la autenticidad de la identidad del ganador mediante entrega de una copia de su documento
					de identidad; la presentación física del ticket de compra que haya ingresado a la web de participación; que firmen conformidad de entrega del Premio así
					como que firmen la cesión de derechos correspondientes a favor del Organizador para que sus Respuestas e identidad, sean publicadas en los medios de
					comunicación que el Organizador estime conveniente, sin que ello genere derecho a retribución alguna.
				</p>
				<p>
					Los premios estarán en exhibición Rca. Argentina esq. Isaac Kostianovsky - Garden Automotores S.A., Casa Central. La transferencia de la propiedad del
					premio será realizada dentro del plazo máximo de siete días de la fecha del sorteo, sin cargo para el beneficiario.
				</p>
				<p>
					<strong>8. CONDICIONES GENERALES:</strong>
					Toda situación no prevista en estas BASES que pudiera derivar un conflicto entre la firma y los participantes podrán ser resueltas a pedido de las partes
					por la “Comisión Nacional de Juegos de Azar” CONAJZAR o quedando expedita la vía jurisdiccional en caso de que los interesados así lo consideren para la
					mejor defensa de sus derechos.
				</p>
				<p>
					El Organizador se reserva el derecho de negar la participación o dar de baja aquellas participaciones que a su solo criterio sean contrarias a la moral o
					las buenas costumbres y que de alguna manera afecten su imagen y/o que describan marcas o productos ajenos al Organizador.
				</p>
				<p>
					No podrán participar de la Promoción aquellos Participantes que no hubieren cumplimentado la mecánica dentro de la Vigencia de la Promoción y que de alguna
					manera no hubieren cumplimentado con los requisitos establecidos en las presentes Bases.
				</p>
				<p>
					Todo impuesto que deba tributarse sobre o en relación con los premios y/o gastos que no estén incluidos específicamente como premios en las presentes
					Bases, estarán exclusivamente a cargo de cada Participante ganador. El Organizador no se hará cargo de ningún gasto extra y/o adicional que el participante
					pueda reclamar.
				</p>
				<p>
					El Organizador no será en ningún caso responsable por las fallas en los equipos de comunicación, de suministro de energía, de líneas telefónicas, de la red
					de Internet, ni por desperfectos técnicos, errores humanos o acciones deliberadas de terceros que pudieran perturbar, suspender o interrumpir el normal
					desarrollo de la Promoción.
				</p>
				<p>
					Los Participantes de esta Promoción, autorizarán al Organizador, como condición para la participación, a utilizar sus nombres, con fines comerciales, en
					los medios de comunicación y formas que el Organizador disponga, sin que esto les otorgue derecho al cobro de suma alguna, durante la Vigencia de la
					Promoción y hasta los tres (3) años desde su finalización.
				</p>
				<p>
					El Organizador podrá ampliar la cantidad de Premios y la Vigencia de la Promoción, cuando circunstancias no imputables al Organizador y no previstas en
					estas Bases o que constituyan caso fortuito o fuerza mayor que lo justifiquen y podrá cancelar, suspender o modificar la Promoción previa comunicación a la
					CONAJZAR para su autorización correspondiente y puesta en conocimiento de los participantes.
				</p>
				<p>
					No se podrá exigir el canje del Premio por dinero u otros bienes o servicios diferentes a los establecidos en estas Bases.
				</p>
				<p>
					La participación en las Promoción implica la aceptación de estas Bases, así como de las decisiones que adopte el Organizador, sobre cualquier cuestión
					prevista o no prevista en ellas y que no signifique alterar la esencia de la Promoción.
				</p>
				<p>
					Está prohibido y será anulado cualquier intento o método de participación en la Promoción que se realice por cualquier proceso, técnica o mecánica de
					participación distinta a las detalladas precedentemente. El Organizador no será responsable por aquellas participaciones que no se reciban a causa de
					fallas de transmisión o técnicas de cualquier tipo no imputables al Organizador.
				</p>
				<p>
					<strong>9. EXCEPCIÓN:</strong>
					Si resultase ganadora de uno de los premios, una persona prófuga de la justicia o que se halle condenada a pena privativa de libertad por sentencia
					judicial, o menor de edad, el sorteo de este premio será anulado y se procederá al llamado del primer ganador suplente.
				</p>
				<p>
					<strong>10. LIMITACIÓN DE RESPONSABILIDAD:</strong>
				</p>
				<p>
					La responsabilidad del Organizador respecto a los ganadores de la presente Promoción estará limitada al cumplimiento de lo establecido en el punto 4 de las
					presentes Bases, quedando expresamente excluida cualquier responsabilidad derivada de eventuales accidentes, daños o perjuicios ocurridos durante el viaje
					de los ganadores.
				</p>
				<p>
					Facebook no patrocina, avala ni administra de modo alguno esta Promoción, ni está asociado a ella. Estás proporcionando tu información as Organizador y no
					a Facebook. La información que proporciones sólo se utilizará para los fines promocionales y comerciales que el Organizador considere pertinente.
				</p>
				<p>
					Las presentes BASES podrán ser solicitadas sin costo alguno al 0800119910 o en la FanPage de REXONA Paraguay.
				</p>
			</div>
		</div>
		<div className="footer col-xs-12">
			<Link to="/" className="btn btn-xs">
				<span className="glyphicon glyphicon-chevron-left"></span>
				Volver al inicio
			</Link>
		</div>
	</div>
)

export default Bases