import React from 'react'
import { Link } from 'react-router'
import request from 'superagent'
import _ from 'lodash'
import DashboardView from 'components/DashboardView'
import DashboardLogin from 'components/DashboardLogin'
import cookie from 'react-cookie'

class Dashboard extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			logged: cookie.load('logged'),
			users: []
		}
		this.onLogin = this.onLogin.bind(this)
	}
	onLogin(pass) {
		if (pass == 'agenciamoderna') {
			cookie.save('logged', true, { path: '/' })
			this.setState({
				logged: true
			})
		}
		else{
			console.error('wrong password')
		}
	}
	componentDidMount() {
		request
			.get('/dashdata')
			.then((response) => {
				if (response.body) {
					const users = response.body.data
					this.setState({
						users
					})
				}
			}, (err) => {
				console.error(err)
			})
	}
	render(){
		const { users, logged } = this.state
		return(
			<div>
				{logged ? <DashboardView users={users} /> : <DashboardLogin onLogin={this.onLogin} />}
			</div>
		)
	}
}

Dashboard.path = '/dashboard'

export default Dashboard