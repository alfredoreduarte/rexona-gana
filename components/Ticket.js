import React from 'react'
import { connect } from 'react-redux'
import { push } from 'react-router-redux'
import { FormGroup, FormControl, Button } from 'react-bootstrap'
import { reduxForm } from 'redux-form'
import FileInput from 'react-file-input'
import { upload } from 'actions/entities'
import { fetchingOn } from 'actions/app'

let Ticket = ({ fields: { photo, date }, name, fetching, handleNext }) => (
	<div>
		<form onSubmit={e => handleNext(e)}>
			<p className="h3 text-center">Sólo un paso más, {name}!</p>
			<p className="h4 text-center">Subí la foto de tu ticket de compra donde se visualice un desodorante Rexona</p>
			<br/>
			<br/>
			<FormGroup bsSize="lg">
				<label>Foto:</label>
				<div className="photo-input">
				<FileInput
					required={true}
					accept=".png,.jpg,.jpeg"
					className="form-control"
					value={null}
					{...photo}
				/>
				</div>
			</FormGroup>
			<br/>
			<br/>
			<FormGroup bsSize="lg">
				<label>Fecha del Ticket</label>
				<FormControl
					required={true}
					type="date"
					placeholder="Fecha"
					{...date}
				/>
			</FormGroup>
			<FormGroup className="text-center" bsSize="lg">
				<Button className="btn-lg text-uppercase" type="submit" disabled={false}>
					{fetching ? 'Cargando...' : 'Continuar' }
				</Button>
			</FormGroup>
		</form>
	</div>
)


Ticket = reduxForm({
	form: 'formData',
	destroyOnUnmount: false,
	fields: ['photo', 'date']
})(Ticket)

const mapStateToProps = state => {
	return {
		fetching: state.app.isFetching,
		name: state.form.formData ? state.form.formData.name.value : 'Juan'
	}
}

const mapDispatchToProps = dispatch => {
	return {
		handleNext: e => {
			e.preventDefault()
			dispatch(upload())
			dispatch(fetchingOn())
		}
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Ticket)