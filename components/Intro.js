import React from 'react'
import { connect } from 'react-redux'
import { push } from 'react-router-redux'
import { FormGroup, FormControl, Button, HelpBlock } from 'react-bootstrap'
import { reduxForm } from 'redux-form'
import Footer from 'components/Footer'

let Intro = ({ fields: { document }, handleNext }) => (
	<div>
		<div className="vertiflex">
			<div className="">
				<div className="car"><img src="/images/car.png" /></div>
				<form className="text-center" onSubmit={e => handleNext(e, document)}>
					<FormGroup bsSize="lg">
						<p className="h4">Ingresá tu número de cédula</p>
						<FormControl
							required={false}
							bsSize="lg"
							type="text"
							{...document}
						/>
						{document.touched && document.error && <HelpBlock>{document.error}</HelpBlock>}
					</FormGroup>
					<FormGroup className="text-center" bsSize="lg">
						<Button className="btn-lg text-uppercase" type="submit">
							<b>Continuar</b>
						</Button>
					</FormGroup>
				</form>
			</div>
			<Footer/>
		</div>
		<hr/>
		<p className="text-center">
			<small>
				Promoción exclusiva del <br/>
				<img src="/images/stock.png" style={{height: '20px'}} /> 
				del 15/08/16 al 31/10/16
			</small>
		</p>
	</div>
)

const validate = values => {
	const errors = {}
	if (!values.document) {
		errors.document = 'Dato Requerido'
	}
	return errors
}

Intro = reduxForm({
	form: 'formData',
	destroyOnUnmount: false,
	fields: ['document'],
	validate,
})(Intro)

const mapDispatchToProps = dispatch => {
	return {
		handleNext: (e, document) => {
			e.preventDefault()
			if (document.valid) {
				dispatch(push('/step2'))
			}
			else{
				console.log('ERROR')
			}
		}
	}
}

export default connect(undefined, mapDispatchToProps)(Intro)