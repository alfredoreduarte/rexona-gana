import React from 'react'
import { connect } from 'react-redux'
import { FormGroup, FormControl, Button } from 'react-bootstrap'
import Footer from 'components/Footer'

const Desktop = () => (
	<div className="">
		<p className="text-center">
			<img src="/images/title.png" className="img-responsive title" style={{margin: 'auto'}} />
		</p>
		<h2 className="text-center"><b>Para visualizar mejor este sitio, <br/>ingresá desde tu celular</b></h2>
		<br />
		<br />
		<p className="text-center">Podés ganar increíbles premios</p>
		<br />
		<br />
		<div className="row">
			<div className="col-sm-6 col-sm-offset-3">
				<div className="row">
					<img src="/images/car-full.png" />
				</div>
			</div>
		</div>
		<Footer />
	</div>
)

export default Desktop