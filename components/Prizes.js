import React from 'react'
import { connect } from 'react-redux'
import { FormGroup, FormControl, Button } from 'react-bootstrap'
import Footer from 'components/Footer'
import { Link } from 'react-router'

const Prizes = () => (
	<div className="">
		<h2 className="text-center"><b>Premios</b></h2>
		<p className="text-center">1 automóvil Marca KIA Modelo Picanto Año 2016 0 km.</p>
		<div className="row">
			<div className="col-sm-6 col-sm-offset-3">
				<div className="row">
					<img src="/images/car-full.png" className="img-responsive" />
				</div>
			</div>
		</div>
		<p className="text-center">Fecha del primer sorteo: <br/><b className="">02/noviembre/2016</b></p>
		<hr/>
		<div className="footer">
			<Link to="/" className="btn btn-xs">
				<span className="glyphicon glyphicon-chevron-left"></span>
				Volver al inicio
			</Link>
		</div>
	</div>
)

export default Prizes