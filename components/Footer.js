import React from 'react'
import { Link } from 'react-router'

const Footer = () => (
	<div className="footer">
		<Link to="/bases" className="btn">Bases y Condiciones</Link>
		<Link to="/prizes" className="btn">Premios</Link>
	</div>
)

export default Footer