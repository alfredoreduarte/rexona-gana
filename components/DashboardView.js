import React from 'react'
import { Link } from 'react-router'
import request from 'superagent'
import _ from 'lodash'

const DashboardView = ({
	users
}) => {
	return(
		<div className="container-fluid dashboard">
			<div className="col-md-12">
				<h1>Ganá con Rexona</h1>
			</div>
			<div className="col-md-12">
				<table className="table">
					<thead>
						<tr>
							<th>Nombre</th>
							<th>Documento</th>
							<th>Teléfono</th>
							<th>Email</th>
							<th>Foto</th>
						</tr>
					</thead>
					<tbody>
						{users.map( (user) => 
							<tr key={user.id}>
								<td>{JSON.parse(user.data).name} {JSON.parse(user.data).lastName}</td>
								<td>{JSON.parse(user.data).document}</td>
								<td>{JSON.parse(user.data).phone}</td>
								<td>{JSON.parse(user.data).email}</td>
								<td>
									<a href={`/uploads/${JSON.parse(user.data).fileName}`} target="_blank">
										<img src={`/uploads/${JSON.parse(user.data).fileName}`} className="img-responsive" width="100px" />
									</a>
								</td>
							</tr>
						)}
					</tbody>
				</table>
			</div>
		</div>
	)
}

export default DashboardView