import React from 'react'
import { connect } from 'react-redux'
import { FormGroup, FormControl, Button } from 'react-bootstrap'
import { reduxForm } from 'redux-form'
import Footer from 'components/Footer'

let Thanks = ({ name, handleNext, coupons }) => (
	<div className="text-center">
		<h1>Felicidades, {name}</h1>
		<br/>
		<br/>
		<p className="h3">Sumaste 1 cupón para participar del sorteo, tenés {coupons} cupones acumulados!</p>
		<p className="h4">Recordá guardar tu ticket para el sorteo</p>
		<hr/>
		<Footer/>
	</div>
)

const mapStateToProps = state => {
	return {
		name: state.form.formData ? state.form.formData.name.value : 'Juan',
		coupons: state.app.couponCount,
	}
}

const mapDispatchToProps = dispatch => {
	return {
		
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Thanks)