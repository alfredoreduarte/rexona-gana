import React from 'react'
import { Link } from 'react-router'
import request from 'superagent'
import _ from 'lodash'

class DashboardLogin extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			password: 'null'
		}
		this.onPassChange = this.onPassChange.bind(this)
	}
	onPassChange(e){
		this.setState({
			password: e.target.value
		})
	}
	render(){
		const { onLogin } = this.props
		return(
			<div className="container-fluid dashboard">
				<form className="col-md-4 col-md-offset-4" onSubmit={e => {
					e.preventDefault()
					onLogin(this.state.password)
				}}>
					<div className="form-group">
						<label>Contraseña</label>
						<input type="text" className="form-control" onChange={this.onPassChange}/>
					</div>
					<button type="submit" className="btn btn-primary">Ingresar</button>
				</form>
			</div>
		)
	}
}

export default DashboardLogin