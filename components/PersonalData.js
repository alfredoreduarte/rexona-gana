import React from 'react'
import { connect } from 'react-redux'
import { push } from 'react-router-redux'
import { FormGroup, FormControl, Button, HelpBlock } from 'react-bootstrap'
import { reduxForm } from 'redux-form'

let PersonalData = ({ fields: { name, lastName, phone, email }, handleNext }) => (
	<div>
		<form className="text-center" onSubmit={e => handleNext(e, {
			name,
			lastName,
			phone,
			email,
		})}>
			<p className="h3">Registrá tus datos para participar</p>
			<FormGroup bsSize="lg">
				<FormControl
					required={false}
					type="text"
					placeholder="Nombre"
					{...name}
				/>
				{name.touched && name.error && <HelpBlock>{name.error}</HelpBlock>}
			</FormGroup>
			<FormGroup bsSize="lg">
				<FormControl
					required={false}
					type="text"
					placeholder="Apellido"
					{...lastName}
				/>
				{lastName.touched && lastName.error && <HelpBlock>{lastName.error}</HelpBlock>}
			</FormGroup>
			<FormGroup bsSize="lg">
				<FormControl
					required={false}
					type="text"
					placeholder="Teléfono"
					{...phone}
				/>
				{phone.touched && phone.error && <HelpBlock>{phone.error}</HelpBlock>}
			</FormGroup>
			<FormGroup bsSize="lg">
				<FormControl
					required={false}
					type="email"
					placeholder="Email"
					{...email}
				/>
				{email.touched && email.error && <HelpBlock>{email.error}</HelpBlock>}
			</FormGroup>
			<FormGroup className="text-center" bsSize="lg">
				<Button className="btn-lg text-uppercase" type="submit">
					<b>Registrarme</b>
				</Button>
			</FormGroup>
		</form>
	</div>
)

const validate = values => {
	const errors = {}
	if (!values.name) {
		errors.name = 'Dato Requerido'
	}
	else if (!values.lastName) {
		errors.lastName = 'Dato Requerido'
	}
	else if (!values.phone) {
		errors.phone = 'Dato Requerido'
	}
	else if (!values.email) {
		errors.email = 'Dato Requerido'
	} else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
		errors.email = 'Email inválido'
	}
	return errors
}

PersonalData = reduxForm({
	form: 'formData',
	destroyOnUnmount: false,
	fields: ['name', 'lastName', 'phone', 'email'],
	validate,
})(PersonalData)

const mapDispatchToProps = dispatch => {
	return {
		handleNext: (e, fields) => {
			// e.preventDefault()
			// dispatch(push('/step3'))
			e.preventDefault()
			if (fields.name.valid && fields.lastName.valid && fields.phone.valid && fields.email.valid) {
				dispatch(push('/step3'))
			}
			else{
				console.log('ERROR')
			}
		}
	}
}

export default connect(undefined, mapDispatchToProps)(PersonalData)