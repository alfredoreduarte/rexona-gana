import React from 'react'
import { Provider } from 'react-redux'
import { Router, Route, IndexRoute, browserHistory } from 'react-router'
import configureStore from 'store/configureStore'
import MobileDetect from 'mobile-detect'

import Root from 'containers/Root'
import Intro from 'components/Intro'
import BrowserRedirection from 'components/BrowserRedirection'
import PersonalData from 'components/PersonalData'
import Ticket from 'components/Ticket'
import Thanks from 'components/Thanks'
import Desktop from 'components/Desktop'
import Bases from 'components/Bases'
import Prizes from 'components/Prizes'
import Dashboard from 'components/Dashboard'

const store = configureStore()

const detectDesktop = (nextState, replace) => {
	const md = new MobileDetect(window.navigator.userAgent)
	if (!md.mobile()) {
	// if (true) {
		replace({
			pathname: `/desktop`,
		})
	}
	else{
		const userAgentString = window.navigator.userAgent
		const isFacebook = userAgentString.indexOf('FBAN') > -1 || userAgentString.indexOf('FBAV') > -1 || userAgentString.indexOf('FB_IAB') > -1
		const isAndroid = userAgentString.indexOf('Android') > -1
		if (isFacebook && isFacebook) {
			replace({
				pathname: `/browserredirection`,
			})
		}
	}
}

const router = (
	<Provider store={store}>
		<Router history={browserHistory}>
			<Route path={Root.path} component={Root}>
				<IndexRoute 
					component={Intro}
					onEnter={detectDesktop} />
				<Route path={'/step2'} component={PersonalData} />
				<Route path={'/browserredirection'} component={BrowserRedirection} />
				<Route path={'/step3'} component={Ticket} />
				<Route path={'/thanks'} component={Thanks} />
				<Route path={'/bases'} component={Bases} />
				<Route path={'/prizes'} component={Prizes} />
			</Route>
			<Route path={'/desktop'} component={Desktop} />
			<Route path={Dashboard.path} component={Dashboard} />
		</Router>
	</Provider>
)

export default router