import 'isomorphic-fetch'
import { push } from 'react-router-redux'
import { getDigestedFormData, getFile } from 'selectors/users'
import { setCouponCount } from 'actions/app'

// import { normalize, arrayOf } from 'normalizr'
// import * as schema from 'schema'
// import * as CONFIG from 'config.dev'
// import Cookies from 'js-cookie'
// import humps from 'humps'

// export const receiveEntities = (entities) => ({
// 	type: 'RECEIVE_ENTITIES',
// 	response: {
// 		entities
// 	}
// })

// export const fetchEntities = () => {
// 	const url = CONFIG.BASE_URL + '/admin_user_properties.json'
// 	const api_key = Cookies.get('api_key')
// 	return dispatch => {
// 		return fetch(url, {
// 					method: 'GET',
// 					headers: {
// 						'Authorization': `Token token="${api_key}"`,
// 					}
// 				})
// 				.then(response => response.json())
// 				.then(json =>{
// 					const camelizedJson = humps.camelizeKeys(json)
// 					console.log(camelizedJson)
// 					const normalized = normalize(camelizedJson, schema.entities)
// 					dispatch(receiveEntities(normalized.entities))
// 				})
// 				.catch(exception =>
// 					console.log('parsing failed', exception)
// 				)
// 	}
// }

export const upload = () => {
	const url = '/upload'
	return (dispatch, getState) => {
		const state = getState()
		const photo = getFile(state)
		const formData = new FormData()
	 	formData.append('photo', photo)
		return fetch(url, {
					method: 'POST',
					body: formData
				})
				.then(response => response.json())
				.then(json =>{
					console.log('json', json)
					dispatch(saveData(json.fileName))
				})
				.catch(exception =>
					console.log('parsing failed', exception)
				)
	}
}

export const saveData = fileName => {
	const url = '/savedata'
	return (dispatch, getState) => {
		const state = getState()
		const formData = Object.assign({}, getDigestedFormData(state), {
			fileName
		})
		return fetch(url, {
					method: 'POST',
					headers: {
						'Accept': 'application/json',
						'Content-Type': 'application/json',
					},
					body: JSON.stringify(formData)
				})
				.then(response => response.json())
				.then(json =>{
					console.log('savedatajson', json)
					dispatch(setCouponCount(json.couponCount))
					dispatch(push(`/thanks`))
				})
				.catch(exception =>
					console.log('parsing failed', exception)
				)
	}
}