export const fetchingOn = () => ({
	type: 'FETCHING_ON'
})
export const fetchingOff = () => ({
	type: 'FETCHING_OFF'
})
export const setCouponCount = count => ({
	type: 'COUPON_COUNT',
	count
})