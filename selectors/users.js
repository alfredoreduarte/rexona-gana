import _ from 'lodash'
import { createSelector } from 'reselect'

export const getRawFormData = state => state.form.formData

export const getFile = createSelector(
	getRawFormData,
	formData => {
		return formData.photo.value[0]
	}
)

export const getDigestedFormData = createSelector(
	getRawFormData,
	formData => {
		return {
			document: formData.document.value,
			name: formData.name.value,
			lastName: formData.lastName.value,
			phone: formData.phone.value,
			email: formData.email.value,
			date: formData.date.value,
		}
	}
)