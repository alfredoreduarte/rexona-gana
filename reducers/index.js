import { combineReducers } from 'redux'
import { reducer as formReducer } from 'redux-form'
import { routerReducer as routing } from 'react-router-redux'

import app from 'reducers/app'

const rootReducer = combineReducers({
	routing,
	app,
	form: formReducer
})

export default rootReducer