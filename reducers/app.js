const app = (state = {
	isFetching: false
}, action) => {
	switch (action.type) {
		case 'FETCHING_ON':
			return Object.assign({}, state, {
				isFetching: true
			})
		case 'FETCHING_OFF':
			return Object.assign({}, state, {
				isFetching: false
			})
		case 'COUPON_COUNT':
			return Object.assign({}, state, {
				couponCount: action.count
			})
		default:
			return state
	}
}

export default app